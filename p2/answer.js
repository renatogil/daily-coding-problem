const p2 = (numbers) => {
  let products = [];
  for (i in numbers) {
    let total = 0;
    for (j in numbers) {
      if (i === j) {
        continue;
      }
      total = (total === 0) ? numbers[j] : (total * numbers[j]);
    }
    products[i] = total;
  }
  console.log(products);
}

console.log(p2([1, 2, 3, 4, 5]));
// [ 120, 60, 40, 30, 24 ]
