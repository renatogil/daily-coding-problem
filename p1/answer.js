const p1 = (candidates, k) => {
  // array of values needed to match the difference
  // between k and current value in array iteration
  const needed = [];
  for (const current of candidates) {
    const diff = k - current
    if (needed[diff]) {
      return true;
    }
    needed[current] = diff
  }

  return false;
}

console.log(p1([10, 15, 3, 7], 10));
// true
