const findFirstMissingPositive = (numbers) => {
  let existences = {};

  numbers.sort().map((number) => {
    if (number > 0) {
      existences[number] = true;
    }
  });

  for (i = 1; ; i++) {
    if (!existences[i]) {
      break;
    }
  }

  console.log(i);
}

findFirstMissingPositive([3, 4, -1, 1]);
// 2

findFirstMissingPositive([1, 2, 0]);
// 3

findFirstMissingPositive([-1, -4, -3]);
// 1
