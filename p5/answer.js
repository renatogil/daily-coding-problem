const cons = (a, b) => {
  const pair = (f) => f(a, b);
  return pair;
};

const car = (first) => first((a, b) => a);
const cdr = (last) => last((a, b) => b);

console.log(car(cons(3, 4)));
// 3

console.log(cdr(cons(3, 4)));
// 4

console.log(cons(3, 4));
// [Function: pair]

console.log(typeof car);
// function

console.log(typeof car(Function));
// function
