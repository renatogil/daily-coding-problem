class Node {
  constructor (val, left, right) {
    this.val = val;
    this.left = left;
    this.right = right;
  }
}

const serialize = (root) => {
  return JSON.stringify(root);
}

const deserialize = (s) => {
  return JSON.parse(s);
}

const assert = (condition, message) => {
    if (!condition) {
        throw message || "Assertion failed";
    }
    return true;
}

const node = new Node('root', new Node('left', new Node('left.left')), new Node('right'));
assert(deserialize(serialize(node)).left.left.val === 'left.left');
// true

console.log(serialize(node));
// {"val":"root","left":{"val":"left","left":{"val":"left.left"}},"right":{"val":"right"}}

console.log(deserialize(serialize(node)));
//{ val: 'root',
//  left: { val: 'left', left: { val: 'left.left' } },
//  right: { val: 'right' } }

console.log(deserialize(serialize(node)).left.left.val);
// left.left
